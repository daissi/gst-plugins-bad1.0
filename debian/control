Source: gst-plugins-bad1.0
Section: libs
Priority: optional
Maintainer: Maintainers of GStreamer packages <gst-plugins-bad1.0@packages.debian.org>
Uploaders: Sebastian Dröge <slomo@debian.org>,
           Sjoerd Simons <sjoerd@debian.org>,
           Marc Leeman <marc.leeman@gmail.com>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-gir,
               flite-dev,
               glslc,
               gstreamer1.0-plugins-base (>= 1.24.0),
               gstreamer1.0-plugins-good (>= 1.24.0),
               gstreamer1.0-tools,
               ladspa-sdk,
               libaom-dev,
               libasound2-dev (>= 0.9.1) [linux-any],
               libass-dev (>= 0.10.4),
               libavtp-dev,
               libbluetooth-dev (>= 5) [linux-any],
               libbs2b-dev (>= 3.1.0),
               libbz2-dev,
               libcairo2-dev,
               libchromaprint-dev,
               libcurl4-gnutls-dev (>= 7.55.0),
               libdc1394-dev (>= 2.2.5) [linux-any],
               libdca-dev,
               libde265-dev (>= 0.9),
               libdirectfb-dev,
               libdrm-dev (>= 2.4.98) [linux-any],
               libdvdnav-dev (>= 4.1.2) [!hurd-any],
               libexempi-dev,
               libexif-dev (>= 0.6.16),
               libfaad-dev (>= 2.7),
               libfluidsynth-dev (>= 1.0),
               libfreeaptx-dev (>= 0.1.1),
               libgirepository1.0-dev (>= 0.9.12-4~),
               libglib2.0-dev (>= 2.68),
               libgme-dev,
               libgnutls28-dev (>= 2.11.3),
               libgsm1-dev,
               libgstreamer-plugins-base1.0-dev (>= 1.24.0),
               libgstreamer1.0-dev (>= 1.24.0),
               libgtk-3-dev (>= 3.15.0),
               libgudev-1.0-dev (>= 143) [linux-any],
               libjson-glib-dev,
               libkate-dev (>= 0.1.7),
               liblc3-dev,
               liblcms2-dev (>= 2.7),
               libldacbt-enc-dev [!s390x !hppa !m68k !powerpc !ppc64 !sparc64],
               liblilv-dev (>= 0.22),
               liblrdf0-dev,
               libltc-dev (>= 1.1.4),
               libmjpegtools-dev (>= 2.0.0),
               libmodplug-dev,
               libmpcdec-dev,
               libneon27-dev,
               libnice-dev (>= 0.1.20),
               libopenal-dev (>= 1:1.14),
               libopencv-dev (>= 3.0.0) [amd64 arm64 armel armhf mips64el ppc64el riscv64 s390x alpha powerpc ppc64 sparc64],
               libopenexr-dev,
               libopenh264-dev (>= 1.3.0),
               libopenjp2-7-dev (>= 2.2),
               libopenmpt-dev,
               libopenni2-dev (>= 0.26) [linux-any],
               libopus-dev (>= 0.9.4),
               liborc-0.4-dev (>= 1:0.4.24),
               libpango1.0-dev (>= 1.22),
               libpng-dev,
               libqrencode-dev,
               librsvg2-dev (>= 2.36.2),
               librtmp-dev,
               libsbc-dev (>= 1.1) [linux-any],
               libsndfile1-dev (>= 1.0.16),
               libsoundtouch-dev (>= 1.5.0),
               libspandsp-dev (>= 0.0.6),
               libsrt-gnutls-dev,
               libsrtp2-dev (>= 2.1),
               libssh2-1-dev (>= 1.4.3),
               libssl-dev (>= 1.1),
               libsvtav1enc-dev,
               libusb-1.0-0-dev [linux-any],
               libva-dev (>= 1.8) [linux-any],
               libvo-aacenc-dev,
               libvo-amrwbenc-dev,
               libvulkan-dev [linux-any],
               libwayland-dev (>= 1.11.0) [linux-any],
               libwebp-dev (>= 0.2.1),
               libwildmidi-dev (>= 0.4.2),
               libwpebackend-fdo-1.0-dev (>= 1.8.0) [linux-any],
               libwpewebkit-2.0-dev [amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x ia64 loong64 powerpc sparc64 x32],
               libx11-dev,
               libx11-xcb-dev,
               libx265-dev,
               libxkbcommon-x11-dev,
               libxml2-dev (>= 2.8),
               libxvidcore-dev,
               libzbar-dev (>= 0.9),
               libzvbi-dev,
               libzxing-dev (>= 1.4.0) [amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x alpha hppa hurd-i386 m68k powerpc ppc64 sparc64],
               meson,
               nettle-dev (>= 3),
               opencv-data [amd64 arm64 armel armhf mips64el ppc64el riscv64 s390x alpha powerpc ppc64 sparc64],
               wayland-protocols (>= 1.4) [linux-any],
               xauth,
               xvfb
Build-Conflicts: libopenaptx-dev
Rules-Requires-Root: no
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/gstreamer-team/gst-plugins-bad1.0.git
Vcs-Browser: https://salsa.debian.org/gstreamer-team/gst-plugins-bad1.0/
Homepage: https://gstreamer.freedesktop.org

Package: gstreamer1.0-plugins-bad-apps
Architecture: any
Section: utils
Depends: gstreamer1.0-plugins-bad (= ${binary:Version}),
         ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends}
Conflicts: pitivi (<< 0.9999)
Description: GStreamer helper programs from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 This package contains helper programs from the "bad" set, a set of
 plug-ins that aren't up to par compared to the rest. They might be
 close to being good quality, but they're missing something - be it a
 good code review, some documentation, a set of tests, a real live
 maintainer, or some actual wide use.

Package: gstreamer1.0-plugins-bad
Architecture: any
Multi-Arch: same
Depends: gstreamer1.0-plugins-base (>= 1.24.0),
         gstreamer1.0-plugins-good (>= 1.24.0),
         libgstreamer-plugins-bad1.0-0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Provides: gstreamer1.0-plugins-bad-faad,
          gstreamer1.0-plugins-bad-videoparsers,
          ${gstreamer:Provides}
Suggests: frei0r-plugins
Conflicts: gstreamer1.0-plugins-bad-faad (<< 1.11.91-1ubuntu1),
           gstreamer1.0-plugins-bad-videoparsers (<< 1.11.91-1ubuntu1)
Replaces: gstreamer1.0-plugins-bad-faad (<< 1.11.91-1ubuntu1),
          gstreamer1.0-plugins-bad-videoparsers (<< 1.11.91-1ubuntu1),
          gstreamer1.0-plugins-base (<< 0.11.94),
          gstreamer1.0-plugins-good (<< 1.1.2)
Breaks: gstreamer1.0-plugins-base (<< 0.11.94),
        gstreamer1.0-plugins-good (<< 1.1.2)
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
XB-GStreamer-URI-Sinks: ${gstreamer:URISinks}
XB-GStreamer-Encoders: ${gstreamer:Encoders}
XB-GStreamer-Decoders: ${gstreamer:Decoders}
Description: GStreamer plugins from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.

Package: gstreamer1.0-opencv
Architecture: amd64 arm64 armel armhf mips64el ppc64el riscv64 s390x alpha powerpc ppc64 sparc64
Multi-Arch: same
Depends: gstreamer1.0-plugins-base,
         libgstreamer-opencv1.0-0 (= ${binary:Version}),
         libgstreamer-plugins-bad1.0-0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Conflicts: gstreamer1.0-plugins-bad (<< 1.17.2~)
Replaces: gstreamer1.0-plugins-bad (<< 1.17.2~)
Recommends: opencv-data
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
XB-GStreamer-URI-Sinks: ${gstreamer:URISinks}
XB-GStreamer-Encoders: ${gstreamer:Encoders}
XB-GStreamer-Decoders: ${gstreamer:Decoders}
Description: GStreamer OpenCV plugins
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains the OpenCV plugins.

Package: gstreamer1.0-wpe
Architecture: amd64 arm64 armel armhf i386 mips64el ppc64el riscv64 s390x ia64 loong64 powerpc sparc64 x32
Multi-Arch: same
Depends: gstreamer1.0-plugins-base,
         libgstreamer-plugins-bad1.0-0 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
XB-GStreamer-Version: ${gstreamer:Version}
XB-GStreamer-Elements: ${gstreamer:Elements}
XB-GStreamer-URI-Sources: ${gstreamer:URISources}
Description: GStreamer WPEWebKit plugin
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains the WPEWebKit plugin.

Package: libgstreamer-plugins-bad1.0-0
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Conflicts: pitivi (<< 0.9999)
Description: GStreamer libraries from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains shared GStreamer libraries from the "bad" set. The API
 is not guaranteed to be stable.

Package: libgstreamer-opencv1.0-0
Architecture: amd64 arm64 armel armhf mips64el ppc64el riscv64 s390x alpha powerpc ppc64 sparc64
Multi-Arch: same
Depends: ${misc:Depends}, ${shlibs:Depends}
Conflicts: libgstreamer-plugins-bad1.0-0 (<< 1.13.1)
Replaces: libgstreamer-plugins-bad1.0-0 (<< 1.13.1)
Description: GStreamer OpenCV libraries
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains shared GStreamer libraries for OpenCV.

Package: libgstreamer-plugins-bad1.0-dev
Architecture: any
Multi-Arch: same
Section: libdevel
Depends: gir1.2-gst-plugins-bad-1.0 (= ${binary:Version}),
         libgstreamer-opencv1.0-0 (= ${binary:Version}) [amd64 arm64 armel armhf mips64el ppc64el riscv64 s390x alpha powerpc ppc64 sparc64],
         libgstreamer-plugins-bad1.0-0 (= ${binary:Version}),
         libgstreamer-plugins-base1.0-dev,
         libgstreamer1.0-dev,
         libopencv-dev (>= 2.3.0) [amd64 arm64 armel armhf mips64el ppc64el riscv64 s390x alpha powerpc ppc64 sparc64],
         ${misc:Depends}
Conflicts: pitivi (<< 0.9999)
Description: GStreamer development files for libraries from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains development files for GStreamer libraries from the
 "bad" set. The API is not guaranteed to be stable.

Package: gir1.2-gst-plugins-bad-1.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: ${gir:Depends}, ${misc:Depends}, ${shlibs:Depends}
Description: GObject introspection data for the GStreamer libraries from the "bad" set
 GStreamer is a streaming media framework, based on graphs of filters
 which operate on media data.  Applications using this library can do
 anything from real-time sound processing to playing videos, and just
 about anything else media-related.  Its plugin-based architecture means
 that new data types or processing capabilities can be added simply by
 installing new plug-ins.
 .
 GStreamer Bad Plug-ins is a set of plug-ins that aren't up to par compared
 to the rest. They might be close to being good quality, but they're missing
 something - be it a good code review, some documentation, a set of tests, a
 real live maintainer, or some actual wide use.
 .
 This package contains introspection data for the GStreamer libraries from
 the "bad" set. It can be used by packages using the GIRepository format to
 generate dynamic bindings.
